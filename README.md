# MyAHFront

Prerequisite : node version 12 minimum

To run the app in production mode, before you have to :
```bash
npm install
npm run build -- --prod
```

A config file for nginx is in `config/default.conf`, you have to update the line 15 for you.

Take the `dist/myAHFront` folder and put into a web server file to serve it

default creds : admin@myah.com / admin
