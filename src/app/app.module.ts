import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { NbAuthJWTToken, NbAuthModule, NbPasswordAuthStrategy } from '@nebular/auth';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  NbDialogModule,
  NbGlobalLogicalPosition,
  NbLayoutModule,
  NbMenuModule,
  NbSidebarModule,
  NbThemeModule,
  NbToastrModule
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AuthGuard } from './services/auth-guard.service';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { environment } from '../environments/environment';
import * as Sentry from '@sentry/angular';
import { Integrations } from '@sentry/tracing';
import { Router } from '@angular/router';

const config: SocketIoConfig = {
  url: environment.socket.url,
  options: {
    path: environment.socket.path
  }
};
Sentry.init({
  dsn: 'https://e42f35622ad54001b97371c4d0b8bdf8@o462316.ingest.sentry.io/5465578',
  integrations: [
    new Integrations.BrowserTracing({
      tracingOrigins: ['localhost', 'https://yourserver.io/api'],
      routingInstrumentation: Sentry.routingInstrumentation
    })
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0
});

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    HttpClientModule,
    SocketIoModule.forRoot(config),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbToastrModule.forRoot({
      destroyByClick: true,
      position: NbGlobalLogicalPosition.BOTTOM_END,
      hasIcon: true
    }),
    NbDialogModule.forRoot({
      autoFocus: true,
      closeOnBackdropClick: true
    }),
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          token: {
            class: NbAuthJWTToken,
            key: 'token'
          },
          baseEndpoint: '',
          login: {
            endpoint: '/api/users/login',
            requireValidToken: false,
            redirect: {
              success: '/content',
              failure: null
            }
          },
          register: {
            endpoint: '/api/users',
            redirect: {
              success: '/content',
              failure: null
            }
          },
          requestPass: {
            endpoint: '/api/users/password/reset',
            method: 'post'
          },
          logout: {
            endpoint: '',
            redirect: {
              success: '/'
            }
          },
          resetPass: {
            endpoint: '/api/users/password/reset',
            method: 'put',
            redirect: {
              success: '/'
            },
            resetPasswordTokenKey: 'reset-token'
          },
          refreshToken: {
            requireValidToken: true,
            method: 'get',
            endpoint: '/api/users/refreshToken'
          }
        })
      ],
      forms: {
        logout: {
          redirectDelay: 0
        }
      }
    }),
    NbToastrModule.forRoot({
      destroyByClick: true,
      position: NbGlobalLogicalPosition.BOTTOM_END,
      hasIcon: true
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
    AuthGuard,
    {
      provide: ErrorHandler,
      useValue: Sentry.createErrorHandler({
        showDialog: true
      })
    },
    {
      provide: Sentry.TraceService,
      deps: [Router]
    },
    {
      provide: APP_INITIALIZER,
      useFactory: () => () => {},
      deps: [Sentry.TraceService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
