import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import {
  NbActionsModule,
  NbAutocompleteModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbContextMenuModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSelectModule,
  NbSidebarModule,
  NbSpinnerModule,
  NbUserModule
} from '@nebular/theme';
import { RouterModule } from '@angular/router';
import { ContentRoutingModule } from './content-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ChangePasswordComponent } from './profile/change-password/change-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TableAuctionatorComponent } from './dashboard/table-auctionator/table-auctionator.component';
import { DetailsComponent } from './details/details.component';
import { ChartsModule } from 'ng2-charts';
import { PriceModule } from '../shared/price/price.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ImportsSelectorModule } from '../shared/imports-selector/imports-selector.module';

@NgModule({
  declarations: [
    ContentComponent,
    DashboardComponent,
    ProfileComponent,
    NavbarComponent,
    ChangePasswordComponent,
    TableAuctionatorComponent,
    DetailsComponent,
    SidebarComponent
  ],
  imports: [
    CommonModule,
    NbLayoutModule,
    RouterModule,
    ContentRoutingModule,
    NbIconModule,
    NbUserModule,
    NbContextMenuModule,
    NbMenuModule,
    ReactiveFormsModule,
    NbSelectModule,
    NbInputModule,
    NbSpinnerModule,
    NbButtonModule,
    ChartsModule,
    NbSearchModule,
    NbAutocompleteModule,
    NbCardModule,
    NbCheckboxModule,
    NbFormFieldModule,
    PriceModule,
    NbSidebarModule,
    NbMenuModule,
    NbActionsModule,
    ImportsSelectorModule
  ],
  providers: []
})
export class ContentModule {}
