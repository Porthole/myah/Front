import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertService, AlertType } from '../../services/alert.service';
import { AuctionatorService } from '../../services/auctionator.service';
import { catchError, finalize, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Subject, throwError } from 'rxjs';
import { CustomErrorCode } from '../../services/custom-error';
import { NbAuthService } from '@nebular/auth';
import { FormBuilder } from '@angular/forms';
import { EventService } from '../../services/event.service';
import { ImportsSelectorService } from '../../shared/imports-selector/imports-selector.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  private readonly _destroy$ = new Subject();

  auctionatorsSelected$ = new Subject<string>();
  auctionator$ = this.auctionatorsSelected$.pipe(
    tap(() => {
      this.loader = true;
    }),
    takeUntil(this._destroy$),
    switchMap((auctionnatorId: string) => {
      return this.auctionatorService.get(auctionnatorId);
    }),
    tap(() => {
      this.loader = false;
    })
  );

  loader = false;

  constructor(
    private alertService: AlertService,
    private auctionatorService: AuctionatorService,
    private authService: NbAuthService,
    private fb: FormBuilder,
    private eventService: EventService,
    private importsSelectorService: ImportsSelectorService
  ) {}

  ngOnInit(): void {
    this.eventService
      .subscribe$('notifications') //
      .subscribe((alert) => {
        if (alert.data?.component === 'dashboard-import' && alert.type === AlertType.Success) {
          this.importsSelectorService.reloadImports.next();
        }
      });
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
    this.auctionatorsSelected$.complete();
  }

  importFiles($event: any) {
    this.loader = true;
    const fileList: FileList = $event.target.files;
    if (fileList.length > 0) {
      const file = fileList[0];
      const ext = file.name.substr(file.name.lastIndexOf('.'), file.name.length);
      if (file.type !== 'text/x-lua' && ext !== '.lua') {
        this.alertService.warn('Bad file type');
        return;
      }
      this.auctionatorService
        .upload(file)
        .pipe(
          catchError((e) => {
            if (e.code === CustomErrorCode.ERRBADREQUEST) {
              this.alertService.warn(e.message, 'Warn');
            } else {
              this.alertService.warn('An error appears, file has not been uploaded.');
            }
            return throwError(e);
          }),
          finalize(() => {
            this.loader = false;
          })
        )
        .subscribe((response) => {
          this.alertService.info('Import is in progress', 'Information');
        });
    }
  }
}
