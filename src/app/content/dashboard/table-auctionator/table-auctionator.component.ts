import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Auctionator, Item } from '../../../services/auctionator.service';
import { FormBuilder } from '@angular/forms';
import { debounceTime, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { toWowPriceString } from '../../../shared/utils/function.utils';

@Component({
  selector: 'app-table-auctionator',
  templateUrl: './table-auctionator.component.html',
  styleUrls: ['./table-auctionator.component.scss']
})
export class TableAuctionatorComponent implements OnInit, OnChanges, OnDestroy {
  // tslint:disable-next-line:variable-name
  private readonly _destroy$ = new Subject();
  @Input() auctionator: Auctionator;
  searchInput = this.fb.control('');
  items: Item[];
  pagedItems: Item[];
  currentPage = 1;
  pages: number[];
  enablePage = true;
  itemsPerPage = 50;
  disablePreviousButton = true;
  disableNextButton = false;

  toWowPriceString = toWowPriceString;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.searchInput.valueChanges
      .pipe(
        takeUntil(this._destroy$),
        debounceTime(800), //
        tap((value: string) => {
          if (value?.length > 2) {
            this.enablePage = false;
            this.pagedItems = this.auctionator.data.filter((i) => {
              return i.name.toLowerCase().indexOf(value.toLowerCase()) !== -1;
            });
          } else {
            this.loadPage(this.currentPage);
            this.enablePage = true;
          }
        })
      )
      .subscribe();
  }

  ngOnChanges(): void {
    this.loadPage(1);
    this.searchInput.reset();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  loadPage(page: number) {
    this.currentPage = page;
    this.searchInput.reset();
    const itemsLength = this.auctionator.data.length;
    this.items = this.auctionator.data.map((i) => i);
    const nbPages = itemsLength % this.itemsPerPage;
    if (page > 1 && page < nbPages) {
      this.disableNextButton = false;
      this.disablePreviousButton = false;
      this.pages = [page - 1, page, page + 1];
    } else if (page === 1) {
      this.disablePreviousButton = true;
      this.pages = [1, 2, 3];
    } else if (page === nbPages) {
      this.pages = [page - 2, page - 1, page];
      this.disableNextButton = true;
    }
    this.pagedItems = this.items.slice((page - 1) * this.itemsPerPage, page * this.itemsPerPage);
  }
}
