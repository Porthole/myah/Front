import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Item, ItemService } from '../../services/item.service';
import { catchError, finalize, first, map, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { FormBuilder } from '@angular/forms';
import { Subject, throwError } from 'rxjs';
import { AlertService } from '../../services/alert.service';
import { NbAuthService } from '@nebular/auth';
import { FavoriteItem, UserService } from '../../services/user.service';
import { Recipe, RecipeService } from '../../services/recipe.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailsComponent implements OnInit, OnDestroy {
  private readonly _destroy$ = new Subject();
  private readonly _favoritesItems$ = new Subject();
  favoritesItems$ = this._favoritesItems$.pipe(
    startWith(null),
    takeUntil(this._destroy$),
    switchMap(() => {
      return this.authService.getToken();
    }),
    tap((token) => {
      this.favItem = token.getPayload().user.favoritesItems.find((i) => i.itemId === this.item._id);
      this.iconStar = this.favItem ? 'star' : 'star-outline';
      this.cdr.markForCheck();
    }),
    map((token) => token.getPayload().user.favoritesItems)
  );
  favItem: FavoriteItem = null;
  realms: string[];
  factions: string[];
  item$ = this.route.params.pipe(
    tap(() => {
      this.loader = true;
      this.realms = [];
      this.factions = [];
    }),
    takeUntil(this._destroy$),
    switchMap((params) => {
      return this.itemService.get(params.id);
    }),
    tap((item) => {
      this.fixedPrice.setValue(item.fixedPrice);
      this.realms = _.uniq(item.imports.map((i) => i.realm));
      this.factions = _.uniq(item.imports.map((i) => i.faction));
      this.data = null;
      this.graph = null;
      this.item = item;
      this.loader = false;
      this.cdr.markForCheck();
      this._favoritesItems$.next();
      this._recipesSub$.next(item.name);
    })
  );
  item: Item;
  selectorGroup = this.fb.group({
    realm: this.fb.control(''),
    faction: this.fb.control(''),
    importsUsed: this.fb.control({ value: 7, disabled: true })
  });
  loader = false;
  data: any[];
  graph: any;
  fixedPrice = this.fb.control('');
  iconStar = 'star-outline';
  loaderFixedPrice = false;
  loaderSetLimitPrice = false;

  private readonly _recipesSub$ = new Subject<string>();
  recipes: Recipe[];

  constructor(
    private route: ActivatedRoute,
    private itemService: ItemService,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private alertService: AlertService,
    private authService: NbAuthService,
    private userService: UserService,
    private recipeService: RecipeService
  ) {}

  ngOnInit(): void {
    this.selectorGroup.valueChanges
      .pipe(
        tap(() => {
          this.loader = true;
        }),
        takeUntil(this._destroy$)
      )
      .subscribe((value) => {
        if (value.realm && value.faction) {
          if (this.selectorGroup.controls.importsUsed.disabled) {
            this.selectorGroup.controls.importsUsed.enable();
            return;
          }
          this.data = this.item.imports.filter((item) => item.realm === value.realm && item.faction === value.faction);
          if (this.data.length < value.importsUsed) {
            this.selectorGroup.controls.importsUsed.setValue(this.data.length);
            return;
          }
          const data = this.data.filter((i, index) => index + value.importsUsed >= this.data.length);
          this.generateGraph(data);
        } else {
          if (this.selectorGroup.controls.importsUsed.enabled) {
            this.selectorGroup.controls.importsUsed.disable();
          }
        }
        this.loader = false;
        this.cdr.detectChanges();
      });

    this._recipesSub$
      .pipe(
        takeUntil(this._destroy$),
        switchMap((name) => this.recipeService.search(name))
      )
      .subscribe((recipes) => {
        this.recipes = recipes;
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  private generateGraph(data: any[]) {
    this.graph = {
      type: 'line',
      data: {
        labels: data.map((i) => {
          const date = new Date(i.createdAt);
          return `${date.toLocaleDateString(navigator.languages[0], {
            weekday: 'long'
          })} ${date.getDate()}/${date.getMonth() + 1} \n ${date
            .toLocaleTimeString()
            .substr(0, date.toLocaleTimeString().length - 3)}`;
        }),
        datasets: [
          {
            label: 'Prices',
            backgroundColor: 'rgb(69,182,255)',
            borderColor: 'rgb(59,98,255)',
            data: data.map((i) => i.price)
          }
        ]
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                suggestedMin: Math.min(...data.map((i) => i.price)) * 0.5,
                suggestedMax: Math.max(...data.map((i) => i.price)) * 1.4
              }
            }
          ]
        }
      }
    };
  }

  allImports() {
    this.selectorGroup.controls.importsUsed.setValue(this.item.imports.length);
  }

  setFavorite(favoritesItems: FavoriteItem[]) {
    const isFavorite = favoritesItems.find((i) => i.itemId === this.item._id);
    this.iconStar = !isFavorite ? 'star' : 'star-outline';
    let remove = true;
    if (isFavorite) {
      favoritesItems.splice(favoritesItems.indexOf(isFavorite), 1);
      this.favItem = null;
    } else {
      remove = false;
      favoritesItems.push({ itemId: this.item._id });
      this.favItem = { itemId: this.item._id };
    }

    this.cdr.markForCheck();
    this.userService
      .updateCurrent({ favoritesItems })
      .pipe(
        catchError((e) => {
          this.alertService.error(e.message, 'Error');
          return throwError(e);
        })
      )
      .subscribe(() => {
        this.alertService.success(remove ? 'Favorites removed !' : 'Favorites added !', 'Success');
      });
  }

  saveFixedPrice(value: number) {
    this.itemService
      .update(this.item._id, { fixedPrice: value })
      .pipe(
        first(),
        tap(() => (this.loaderFixedPrice = true)),
        finalize(() => (this.loaderFixedPrice = false)),
        catchError((e) => {
          this.alertService.error(e.message);
          return throwError(e);
        })
      )
      .subscribe();
  }

  saveLimitPrice(favoritesItems: FavoriteItem[], price: number) {
    const isFavorite = favoritesItems.find((i) => i.itemId === this.item._id);
    isFavorite.priceLimit = price;

    this.userService
      .updateCurrent({ favoritesItems })
      .pipe(
        catchError((e) => {
          this.alertService.error(e.message, 'Error');
          return throwError(e);
        })
      )
      .subscribe((user) => {
        this._favoritesItems$.next();
        this.alertService.success('Price saved !', 'Success');
      });
  }

  goToRecipe($event: any) {
    this.router.navigate(['/', 'content', 'recipe', $event]);
  }
}
