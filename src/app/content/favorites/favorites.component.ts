import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { throwError } from 'rxjs';
import { AuctionatorService } from '../../services/auctionator.service';
import { catchError, first, switchMap, tap } from 'rxjs/operators';
import { NbAuthService } from '@nebular/auth';
import { Item, ItemService, MergedItem } from '../../services/item.service';
import { FavoriteItem, UserService } from '../../services/user.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavoritesComponent implements OnInit {
  loader = false;
  favoritesItems: MergedItem[];
  tmpFavItems: FavoriteItem[];
  analyzed = false;

  constructor(
    private authService: NbAuthService,
    private auctionatorService: AuctionatorService,
    private itemService: ItemService,
    private cdr: ChangeDetectorRef,
    private userService: UserService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.authService
      .getToken()
      .pipe(
        first(),
        switchMap((token) => {
          this.tmpFavItems = token.getPayload().user.favoritesItems;
          return this.itemService.getAll({ _id: token.getPayload().user.favoritesItems.map((i) => i.itemId) });
        }),
        tap((items: Item[]) => {
          this.favoritesItems = this.tmpFavItems.map((i) => {
            const item = items.find((ii) => ii._id === i.itemId);
            return { ...i, ...item };
          }) as MergedItem[];
          this.cdr.markForCheck();
        })
      )
      .subscribe();
  }

  analyseImport(id: string) {
    this.analyzed = true;
    this.auctionatorService
      .analyse(id)
      .pipe(first())
      .subscribe((analyse) => {
        analyse.favoritesItems.forEach((i) => {
          const item = this.favoritesItems.find((ii) => ii.itemId === i.itemId);
          item.underPrice = !!i.underPrice;
        });
        // @ts-ignore
        this.favoritesItems.sort((a, b) => b.underPrice - a.underPrice);
        this.cdr.markForCheck();
      });
  }

  saveLimitPrice(item: FavoriteItem, price: number) {
    const favitem = this.favoritesItems.find((i) => i.itemId === item.itemId);
    if (favitem) {
      favitem.priceLimit = price;
    }
    this.userService
      .updateCurrent({ favoritesItems: this.favoritesItems })
      .pipe(
        catchError((e) => {
          this.alertService.error(e.message, 'Error');
          return throwError(e);
        })
      )
      .subscribe((user) => {
        this.alertService.success('Price saved !', 'Success');
      });
  }
}
