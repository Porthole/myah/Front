import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritesComponent } from './favorites.component';
import { FavoritesRoutingModule } from './favorites-routing.module';
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbFormFieldModule,
  NbIconModule,
  NbLayoutModule,
  NbListModule,
  NbSelectModule,
  NbSpinnerModule
} from '@nebular/theme';
import { PriceModule } from '../../shared/price/price.module';
import { ImportsSelectorModule } from '../../shared/imports-selector/imports-selector.module';

@NgModule({
  declarations: [FavoritesComponent],
  imports: [
    CommonModule,
    FavoritesRoutingModule,
    NbCardModule,
    NbLayoutModule,
    NbSelectModule,
    NbSpinnerModule,
    NbFormFieldModule,
    NbListModule,
    PriceModule,
    NbAlertModule,
    NbIconModule,
    NbButtonModule,
    ImportsSelectorModule
  ]
})
export class FavoritesModule {}
