import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NbAuthService } from '@nebular/auth';
import { debounceTime, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { ItemService } from '../../services/item.service';
import { FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { NbMenuItem, NbMenuService, NbSidebarComponent } from '@nebular/theme';
import { EventService } from '../../services/event.service';
import { Alert, AlertService, AlertType } from '../../services/alert.service';
import { User } from '../../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit, OnDestroy {
  constructor(
    private authService: NbAuthService,
    private itemService: ItemService,
    private router: Router,
    private eventService: EventService,
    private alertService: AlertService,
    private nbMenuService: NbMenuService,
    private cdr: ChangeDetectorRef
  ) {}

  @Input() sidebar: NbSidebarComponent;
  private readonly _destroy$ = new Subject();

  searchControl = new FormControl('');

  items$ = this.searchControl.valueChanges.pipe(
    debounceTime(500),
    filter((value) => value.length > 2),
    switchMap((value) => {
      return this.itemService.search(value);
    })
  );

  user$: Observable<User> = this.authService.onTokenChange().pipe(
    takeUntil(this._destroy$),
    map((token) => {
      if (token.isValid()) {
        return token.getPayload().user;
      }
      return null;
    })
  );

  notifications = [];
  emptyNotification: NbMenuItem[] = [
    {
      title: 'No notification for the moment'
    }
  ];

  profileMenu: NbMenuItem[] = [
    {
      title: 'Profile',
      icon: 'person-outline',
      link: '/content/profile'
    },
    {
      title: 'Log out',
      icon: 'log-out',
      link: '/logout'
    }
  ];

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
    this.eventService.unsubscribe('notifications');
  }

  ngOnInit(): void {
    this.alertService.alert$
      .pipe(takeUntil(this._destroy$)) //
      .subscribe((alert) => {
        this.notifications.push(this.alertService.notifFromAlert(alert));
        this.cdr.markForCheck();
      });

    this.eventService
      .subscribe$('notifications')
      .pipe(
        tap((alert: Alert) => {
          this.alertService.alert(alert);
        })
      )
      .subscribe();

    this.nbMenuService
      .onItemClick()
      .pipe(takeUntil(this._destroy$))
      .subscribe((item) => {
        this.notifications.splice(this.notifications.indexOf(item.item), 1);
        this.cdr.markForCheck();
      });
  }

  onSelectionChange($event: any) {
    if ($event) {
      this.router.navigate(['content', 'item', $event]);
      this.searchControl.setValue('');
    }
  }

  worstStatus() {
    const status = this.notifications.map((n) => n.status);
    if (status.indexOf(AlertType.Error) !== -1) {
      return 'danger';
    }
    if (status.indexOf(AlertType.Warning) !== -1) {
      return 'warning';
    }
    if (status.indexOf(AlertType.Info) !== -1) {
      return 'info';
    }
    return 'success';
  }
}
