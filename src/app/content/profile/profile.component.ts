import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AlertService } from '../../services/alert.service';
import { NbAuthService } from '@nebular/auth';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnDestroy {
  profileForm = new FormGroup({
    email: new FormControl(),
    emailing: new FormControl()
  });
  private _currentUser$ = this.authService.getToken().pipe(map((token) => token.getPayload().user));
  private _destroy$ = new Subject();

  constructor(
    private authService: NbAuthService,
    private userService: UserService,
    private alertService: AlertService
  ) {
    this._currentUser$
      .pipe(
        takeUntil(this._destroy$),
        tap((user: any) => {
          this.profileForm.patchValue(user);
        })
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  saveUser() {
    const resultForm = this.profileForm.value;
    Object.keys(resultForm).forEach((key) => resultForm[key] === null && delete resultForm[key]);
    this.userService
      .updateCurrent(resultForm)
      .pipe(
        switchMap(() => {
          return this.authService.refreshToken('email');
        })
      )
      .subscribe((user: any) => {
        this.alertService.success('User changes saved !', 'Success');
      });
  }
}
