import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { merge, Observable, Subject } from 'rxjs';
import { Recipe, RecipeService } from '../../services/recipe.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { AuctionatorService } from '../../services/auctionator.service';
import { toWowPriceString } from '../../shared/utils/function.utils';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeDetailComponent implements OnInit, OnDestroy {
  private readonly _destroy$ = new Subject();

  constructor(
    private recipeService: RecipeService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private auctionatorService: AuctionatorService,
    private cdr: ChangeDetectorRef
  ) {}

  toWowPriceString = toWowPriceString;

  private readonly _auctionatorRecipesSub$ = new Subject<string>();
  private _auctionatorRecipes$: Observable<Recipe> = this._auctionatorRecipesSub$.pipe(
    switchMap((auctionatorId) => {
      const recipeId = this.route.snapshot.params.id;
      return this.recipeService.checkRecipeImport(recipeId, auctionatorId);
    })
  );

  private _routeRecipes$ = this.recipeService.get(this.route.snapshot.params.id);

  recipe$: Observable<Recipe> = merge(this._routeRecipes$, this._auctionatorRecipes$).pipe(
    tap(() => this.cdr.markForCheck())
  );

  selectorGroup = this.fb.group({
    faction: '',
    realm: ''
  });
  loader = false;

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  totalCost(recipe: Recipe) {
    let total = 0;
    recipe.components.forEach((c) => {
      total += (c.priceData || 0) * (c.quantity || 1);
    });
    return total;
  }

  checkRecipe(auctionatorId: string) {
    this._auctionatorRecipesSub$.next(auctionatorId);
  }
}
