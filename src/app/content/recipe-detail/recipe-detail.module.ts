import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecipeDetailRoutingModule } from './recipe-detail-routing.module';
import { RecipeDetailComponent } from './recipe-detail.component';
import { NbCardModule, NbInputModule, NbSelectModule, NbSpinnerModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { ImportsSelectorModule } from '../../shared/imports-selector/imports-selector.module';

@NgModule({
  declarations: [RecipeDetailComponent],
  imports: [
    CommonModule,
    RecipeDetailRoutingModule,
    NbCardModule,
    ReactiveFormsModule,
    NbSelectModule,
    NbSpinnerModule,
    NbInputModule,
    ChartsModule,
    ImportsSelectorModule
  ]
})
export class RecipeDetailModule {}
