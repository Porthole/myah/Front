import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Item, ItemService } from '../../../services/item.service';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, debounceTime, finalize, first, switchMap, tap } from 'rxjs/operators';
import { Profession, RecipeService } from '../../../services/recipe.service';
import { NbDialogRef } from '@nebular/theme';
import { AlertService } from '../../../services/alert.service';
import { clearNullValues } from '../../../shared/utils/function.utils';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-recipes-create-modal',
  templateUrl: './recipes-create-modal.component.html',
  styleUrls: ['./recipes-create-modal.component.scss']
})
export class RecipesCreateModalComponent implements OnInit {
  recipForm = this.fb.group({
    item: this.fb.group(
      {
        _id: null,
        name: this.fb.control('', Validators.required),
        fixedPrice: null
      },
      Validators.required
    ),
    itemResult: this.fb.group(
      {
        _id: null,
        name: this.fb.control('', Validators.required),
        quantity: null
      },
      Validators.required
    ),
    profession: this.fb.control(null, Validators.required),
    components: this.fb.array([], this.validateArrayMinLength(1))
  });
  tmpMainName: string;
  tmpResultName: string;
  tmpNames: string[] = [];

  items$ = (this.recipForm.controls.item as FormGroup).controls.name.valueChanges.pipe(
    debounceTime(500),
    switchMap((value) => {
      if (value.length > 2) {
        return this.itemService.search(value);
      } else {
        return of([]);
      }
    })
  );
  itemsResult$ = (this.recipForm.controls.itemResult as FormGroup).controls.name.valueChanges.pipe(
    debounceTime(500),
    switchMap((value) => {
      if (value.length > 2) {
        return this.itemService.search(value);
      } else {
        return of([]);
      }
    })
  );
  professions = Object.values(Profession).map((p) => p.toString());

  private readonly _itemsSub$$: Subject<string>[] = [];
  items$$: Observable<Item[]>[] = [];
  loader = false;

  constructor(
    private fb: FormBuilder,
    private itemService: ItemService,
    private recipeService: RecipeService,
    private alertService: AlertService,
    private userService: UserService,
    private nbDialogRef: NbDialogRef<RecipesCreateModalComponent>
  ) {}

  ngOnInit(): void {}

  get components() {
    return this.recipForm.controls.components as FormArray;
  }

  addComponent() {
    this.components.push(
      this.fb.group({
        _id: null,
        name: this.fb.control('', Validators.required),
        fixedPrice: null,
        quantity: this.fb.control(1, Validators.required)
      })
    );
    const index = this.components.length - 1;

    this._itemsSub$$[index] = new Subject<string>();
    this.items$$[index] = this._itemsSub$$[index].pipe(
      debounceTime(500),
      switchMap((value) => {
        if (value.length > 2) {
          return this.itemService.search(value);
        } else {
          return of([]);
        }
      })
    );
  }

  componentPriceChange(i: number, $event: number) {
    this.components.at(i).patchValue({ fixedPrice: $event });
  }

  removeComponent(i: number) {
    this.components.removeAt(i);
  }

  searchOn($event: Event, i: number) {
    this._itemsSub$$[i].next(($event.target as HTMLInputElement).value);
  }

  saveRecipe() {
    let tmpRecipe = null;
    this.recipeService
      .create(clearNullValues(this.recipForm.value))
      .pipe(
        first(),
        tap(() => (this.loader = true)),
        switchMap((recipe) => {
          this.alertService.success('Recipe saved', 'Success');
          tmpRecipe = recipe;
          return this.userService.refreshToken();
        }),
        finalize(() => (this.loader = false)),
        catchError((err) => {
          this.alertService.error(err.message, 'Error');
          return throwError(err);
        })
      )
      .subscribe(() => {
        this.close(tmpRecipe);
      });
  }

  close(data?: any) {
    this.nbDialogRef.close(data);
  }

  setItem(itemId: string, i?: number) {
    if (itemId && itemId.match(/^[a-f0-9]{24}$/)) {
      this.itemService
        .get(itemId)
        .pipe(first())
        .subscribe((item) => {
          if (i !== undefined) {
            if (i >= 0) {
              this.tmpNames[i] = item.name;
              this.components.at(i).patchValue(item);
            } else {
              this.tmpResultName = item.name;
              this.recipForm.patchValue({ itemResult: item });
            }
          } else {
            this.tmpMainName = item.name;
            this.recipForm.patchValue({ item });
          }
        });
    }
  }

  checkName(i?: number) {
    if (typeof i === 'number') {
      if (i > 0) {
        if (this.components.at(i).value.name !== this.tmpNames[i]) {
          this.components.at(i).patchValue({ _id: null });
        }
      } else {
        if (this.recipForm.value.itemResult.name !== this.tmpResultName) {
          this.recipForm.patchValue({ itemResult: { _id: null } });
        }
      }
    } else {
      if (this.recipForm.value.item.name !== this.tmpMainName) {
        this.recipForm.patchValue({ item: { _id: null } });
      }
    }
  }

  private validateArrayMinLength(minLength: number) {
    return (control: AbstractControl): { [key: string]: string } => {
      return (control as FormArray).length < minLength ? { err: 'invalid length form array' } : null;
    };
  }
}
