import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { RecipesCreateModalComponent } from './recipes-create-modal/recipes-create-modal.component';
import { Recipe, RecipeService } from '../../services/recipe.service';
import { catchError, debounceTime, first, map, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import { AuctionatorService } from '../../services/auctionator.service';
import { Subject, throwError } from 'rxjs';
import { toWowPriceString } from '../../shared/utils/function.utils';
import { FormControl } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { NbAuthService } from '@nebular/auth';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipesComponent implements OnInit, OnDestroy {
  private readonly _destroy$ = new Subject();
  private readonly _recipesSub$ = new Subject<string>();
  recipes: Recipe[] = [];
  loaderRecipe = false;
  searchControl = new FormControl('');
  toWowPriceString = toWowPriceString;
  recipes$ = this.searchControl.valueChanges.pipe(
    takeUntil(this._destroy$),
    debounceTime(200),
    switchMap((value) => this.recipeService.search(value)),
    map((recipes) => {
      return recipes.filter((r) => !this.recipes.find((rr) => rr._id === r._id));
    })
  );

  constructor(
    private dialogService: NbDialogService,
    private recipeService: RecipeService,
    private userService: UserService,
    private authService: NbAuthService,
    private auctionatorService: AuctionatorService,
    private cdr: ChangeDetectorRef,
    private alertService: AlertService
  ) {}

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  openModal() {
    this.dialogService.open(RecipesCreateModalComponent).onClose.subscribe((recipe) => {
      if (recipe) {
        this._recipesSub$.next();
      }
    });
  }

  ngOnInit(): void {
    this._recipesSub$
      .pipe(
        startWith(null),
        tap(() => (this.loaderRecipe = true)),
        takeUntil(this._destroy$),
        switchMap(() => this.recipeService.getMine()),
        tap(() => (this.loaderRecipe = false)),
        tap((recipes) => {
          this.recipes = recipes;
          this.cdr.markForCheck();
        })
      )
      .subscribe();
  }

  checkRecipe(auctionatorId: string) {
    this.auctionatorService
      .recipes(auctionatorId)
      .pipe(first())
      .subscribe((response) => {
        this.recipes = response.recipes;
        this.cdr.markForCheck();
      });
  }

  totalCost(recipe: Recipe) {
    let total = 0;
    recipe.components.forEach((c) => {
      total += (c.priceData || 0) * (c.quantity || 1);
    });
    return total;
  }

  addToSaved(recipe: Recipe) {
    if (recipe) {
      const recipeId = recipe._id;
      this.authService
        .getToken()
        .pipe(
          first(),
          switchMap((token) => {
            const user = token.getPayload().user;
            user.savedRecipes.push(recipeId);
            return this.userService.updateCurrent({
              savedRecipes: user.savedRecipes
            });
          }),
          catchError((e) => {
            this.alertService.error(e.message);
            return throwError(e);
          })
        )
        .subscribe((user) => {
          this._recipesSub$.next();
          this.alertService.success('Correctly added to saved recipes');
        });
    }
  }

  removeSaved(recipe: Recipe) {
    this.authService
      .getToken()
      .pipe(
        first(),
        switchMap((token) => {
          const user = token.getPayload().user;
          user.savedRecipes.splice(user.savedRecipes.indexOf(recipe._id), 1);
          return this.userService.updateCurrent({
            savedRecipes: user.savedRecipes
          });
        }),
        catchError((e) => {
          this.alertService.error(e.message);
          return throwError(e);
        })
      )
      .subscribe((user) => {
        this._recipesSub$.next();
      });
  }
}
