import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesComponent } from './recipes.component';
import { RecipesRoutingModule } from './recipes-routing.module';
import {
  NbAutocompleteModule,
  NbButtonModule,
  NbCardModule,
  NbDialogModule,
  NbFormFieldModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbSelectModule,
  NbSpinnerModule
} from '@nebular/theme';
import { RecipesCreateModalComponent } from './recipes-create-modal/recipes-create-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PriceModule } from '../../shared/price/price.module';
import { ImportsSelectorModule } from '../../shared/imports-selector/imports-selector.module';

@NgModule({
  declarations: [RecipesComponent, RecipesCreateModalComponent],
  imports: [
    CommonModule,
    RecipesRoutingModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbInputModule,
    NbFormFieldModule,
    NbLayoutModule,
    NbDialogModule.forChild(),
    ReactiveFormsModule,
    PriceModule,
    NbAutocompleteModule,
    NbSelectModule,
    NbSpinnerModule,
    ImportsSelectorModule
  ]
})
export class RecipesModule {}
