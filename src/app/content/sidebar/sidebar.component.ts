import { Component, Input } from '@angular/core';
import { NbMenuItem, NbSidebarComponent } from '@nebular/theme';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  @Input() sidebar: NbSidebarComponent;
  constructor() {}

  items: NbMenuItem[] = [
    {
      title: 'Dashboard',
      icon: 'home-outline',
      link: '/content/dashboard'
    },
    {
      title: 'Favorites',
      icon: 'star-outline',
      link: '/content/favorites'
    },
    {
      title: 'Recipes',
      icon: 'layers-outline',
      link: '/content/recipes'
    }
  ];
}
