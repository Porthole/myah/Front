import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { NbToastrService } from '@nebular/theme';

export class Alert {
  title?: string;
  message: string;
  type: AlertType;
  data?: any;
}

export enum AlertType {
  Success = 'success',
  Error = 'error',
  Info = 'info',
  Warning = 'warn'
}

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private subject = new Subject<Alert>();
  private keepAfterRouteChange = false;

  constructor(private toastrService: NbToastrService) {}

  get alert$(): Observable<any> {
    return this.subject.asObservable();
  }

  success(message: string, title?: string) {
    this.toastrService.success(message, title);
    this.subject.next({ type: AlertType.Success, message, title });
  }

  error(message: string, title?: string) {
    this.toastrService.danger(message, title);
    this.subject.next({ type: AlertType.Error, message, title });
  }

  info(message: string, title?: string) {
    this.toastrService.info(message, title);
    this.subject.next({ type: AlertType.Info, message, title });
  }

  warn(message: string, title?: string) {
    this.toastrService.warning(message, title);
    this.subject.next({ type: AlertType.Warning, message, title });
  }

  alert(alert: Alert) {
    switch (alert.type) {
      case AlertType.Error:
        this.error(alert.message, alert.title);
        break;
      case AlertType.Info:
        this.info(alert.message, alert.title);
        break;
      case AlertType.Success:
        this.success(alert.message, alert.title);
        break;
      case AlertType.Warning:
        this.warn(alert.message, alert.title);
        break;
    }
  }

  notifFromAlert(alert: Alert) {
    return {
      title: alert.message,
      icon: this._getIcon(alert.type),
      status: alert.type
    };
  }

  private _getIcon(type: AlertType) {
    switch (type) {
      case AlertType.Success:
        return 'checkmark-circle-outline';
      case AlertType.Error:
        return 'close-circle-outline';
      case AlertType.Info:
        return 'info-outline';
      case AlertType.Warning:
        return 'alert-circle-outline';
    }
  }
}
