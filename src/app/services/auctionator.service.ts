import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';
import { NbAuthService } from '@nebular/auth';
import { Recipe } from './recipe.service';

export interface Auctionator {
  _id: string;
  realm: string;
  faction: string;
  data: Item[];
  userId: string;
  createdAt: Date;
}

export interface Item {
  name: string;
  itemId: string;
  idImport: string;
  price: number;
}

export interface AnalyseRecipe {
  auctionatorId: string;
  faction: string;
  realm: string;
  recipes: Recipe[];
}

@Injectable({
  providedIn: 'root'
})
export class AuctionatorService {
  constructor(private http: HttpClient, private authService: NbAuthService) {}

  upload(file: File) {
    const formData = new FormData();
    formData.append('data', file, file.name);
    return this.http.post('/api/auctionators', formData);
  }

  getAll(params?: any): Observable<Auctionator[]> {
    return this.http.get<any[]>('/api/auctionators', { params });
  }

  get(id: string): Observable<Auctionator> {
    return this.http.get<Auctionator>('/api/auctionators/' + id);
  }

  getMine(): Observable<Auctionator[]> {
    return this.authService.getToken().pipe(
      first(),
      switchMap((token) =>
        this.http.get<Auctionator[]>('/api/auctionators', { params: { userId: token.getPayload().user._id } })
      )
    );
  }

  analyse(id: string) {
    return this.http.get<any>('/api/auctionators/' + id + '/analyse');
  }

  recipes(id: string): Observable<AnalyseRecipe> {
    return this.http.get<AnalyseRecipe>('/api/auctionators/' + id + '/recipes');
  }

  realms(): Observable<string[]> {
    return this.http.get<string[]>('/api/realms');
  }

  factions(): Observable<string[]> {
    return this.http.get<string[]>('/api/factions');
  }
}
