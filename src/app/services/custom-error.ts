export enum CustomErrorCode {
  ERRNOTFOUND = 'ERRNOTFOUND',
  ERRBADREQUEST = 'ERRBADREQUEST',
  ERRINTERNALSERVER = 'ERRINTERNALSERVER',
  ERRNOCONF = 'ERRNOCONF',
  ERRFORBIDDEN = 'ERRFORBIDDEN',
  ERRUNAUTHORIZED = 'ERRUNAUTHORIZED'
}

export class CustomError {
  name = CustomError.name;
  code: CustomErrorCode;
  message: string;
  cause: any;

  constructor(code: CustomErrorCode, message: string, cause?: any) {
    this.code = code;
    this.message = message;
    this.cause = cause;
  }
}
