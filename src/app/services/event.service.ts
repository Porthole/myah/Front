import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable, Subject } from 'rxjs';
import { NbAuthService } from '@nebular/auth';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private _listeners$: { [k: string]: Subject<any> } = {};

  constructor(private socket: Socket, private authService: NbAuthService) {}

  getObs$(topic: string) {
    return this._listeners$[topic].asObservable();
  }

  subscribe$(topic: string): Observable<any> {
    return this.authService.getToken().pipe(
      switchMap((token) => {
        if (this._listeners$[topic]) {
          return this._listeners$[topic].asObservable();
        }

        this.socket.emit(
          'subscribe',
          {
            userId: token.getPayload().user._id,
            topic
          },
          (err) => {
            if (err) {
              console.error(err);
            }
          }
        );
        this._listeners$[topic] = new Subject();
        this.socket.on(topic, (msg) => this._listeners$[topic].next(msg));
        return this._listeners$[topic].asObservable();
      })
    );
  }

  unsubscribe(topic: string) {
    this.socket.emit(
      'subscribe',
      {
        topic
      },
      (err) => {
        if (err) {
          console.error(err);
          return err;
        }
        this.socket.removeAllListeners(topic);
        this._listeners$[topic].complete();
      }
    );
  }
}
