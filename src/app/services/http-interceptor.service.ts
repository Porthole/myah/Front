import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { NbAuthService } from '@nebular/auth';
import { Router } from '@angular/router';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  constructor(private authService: NbAuthService, private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.getToken().pipe(
      mergeMap((token, i) => {
        request = request.clone({
          headers: request.headers.set('Authorization', 'Bearer ' + token.toString())
        });

        request = request.clone({
          headers: request.headers.set('Accept', 'application/json')
        });

        return next.handle(request).pipe(
          catchError((err) => {
            if (err.status === 401) {
              // auto logout if 401 response returned from api

              this.router.navigate(['/logout']);
            }

            const error = err.error || err.statusText;
            return throwError(error);
          })
        );
      })
    );
  }
}
