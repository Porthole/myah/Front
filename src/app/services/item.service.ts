import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FavoriteItem } from './user.service';

export interface Item {
  _id: string;
  name: string;
  imports: {
    _id: string;
    price: number;
    idImport: number;
    realm: string;
    faction: string;
    userId: string;
    createdAt: Date;
  }[];
  fixedPrice?: number;
}

export interface MergedItem extends Item, FavoriteItem {}

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  constructor(private http: HttpClient) {}

  getAll(params?: any): Observable<Item[]> {
    return this.http.get<any[]>('/api/items', { params });
  }

  search(name: string): Observable<Item[]> {
    return this.http.get<any[]>('/api/items/search', { params: { name } });
  }

  get(id: string): Observable<Item> {
    if (!id) {
      return null;
    }
    return this.http.get<Item>('/api/items/' + id);
  }

  update(id: string, itemData: { fixedPrice: number }) {
    return this.http.put('/api/items/' + id, itemData);
  }
}
