import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Item } from './item.service';
import { NbAuthService } from '@nebular/auth';
import { first, switchMap } from 'rxjs/operators';

export interface RecipeItem extends Item {
  quantity: number;
  priceData?: number;
}

export interface Recipe {
  _id: string;
  profession: Profession;
  components: RecipeItem[];
  createdAt?: Date;
  public?: boolean;
  createdBy: string;
  item: RecipeItem;
  itemResult: RecipeItem;
}

export enum Profession {
  ALCHEMY = 'Alchemy',
  BLACKSMITHING = 'Blacksmithing',
  COOKING = 'Cooking',
  ENCHANTING = 'Enchanting',
  ENGINNEERING = 'Engineering',
  HERBALISM = 'Herbalism',
  FIRST_AID = 'First Aid',
  FISHING = 'Fishing',
  MINING = 'Mining',
  SKINNING = 'Skinning',
  TAILORING = 'Tailoring',
  LEATHERWORKING = 'Leatherworking'
}

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  constructor(private http: HttpClient, private authService: NbAuthService) {}

  getAll(params?: any): Observable<Recipe[]> {
    return this.http.get<Recipe[]>('/api/recipes', { params });
  }

  getMine(): Observable<Recipe[]> {
    return this.authService.getToken().pipe(
      first(),
      switchMap((token) => {
        const recipes = [...(token.getPayload().user.savedRecipes || [])];
        if (recipes.length) {
          return this.http.get<Recipe[]>('/api/recipes', { params: { _id: recipes } });
        }
        return of([]);
      })
    );
  }

  get(id: string) {
    return this.http.get<Recipe>('/api/recipes/' + id);
  }

  create(recipeData: Recipe): Observable<any> {
    return this.http.post<any>('/api/recipes', recipeData);
  }

  search(name: string): Observable<Recipe[]> {
    return this.http.get<Recipe[]>('/api/recipes/search', { params: { name } });
  }

  update(id: string, recipeData: Recipe) {
    return this.http.put('/api/recipes/' + id, recipeData);
  }

  checkRecipeImport(recipeId: string, auctionatorId: string) {
    return this.http.get<Recipe>('/api/recipes/' + recipeId + '/auctionator/' + auctionatorId);
  }
}
