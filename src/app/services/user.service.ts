import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';
import { NbAuthService } from '@nebular/auth';

export interface FavoriteItem {
  itemId: string;
  priceLimit?: number;
  underPrice?: boolean;
}

export interface User {
  id: string;
  username: string;
  email: string;
  createdAt: Date;
  updatedAt: Date;
  emailing: boolean;
  roles: string[];
  enabled: true;
  lastLogin: Date;
  favoritesItems: FavoriteItem[];
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private httpClient: HttpClient, private authService: NbAuthService) {}

  get(id: string, criteria = {} as any) {
    return this.httpClient.get('/api/users/' + id, { params: criteria }) as Observable<User>;
  }

  getCurrent() {
    return this.httpClient.get('/api/users/current') as Observable<User>;
  }

  getAll(criteria = {} as any) {
    return this.httpClient.get('/api/users', { params: criteria }) as Observable<User[]>;
  }

  create(user) {
    return this.httpClient.post('/api/users', user) as Observable<User>;
  }

  update(id: string, user: any) {
    return this.httpClient.put('/api/users/' + id, user) as Observable<User>;
  }

  updateCurrent(userData) {
    return this.httpClient.put('/api/users/current', userData).pipe(
      first(),
      switchMap(() => this.refreshToken())
    );
  }

  refreshToken() {
    return this.authService.refreshToken('email');
  }
}
