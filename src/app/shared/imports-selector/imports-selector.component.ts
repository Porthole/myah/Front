import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Auctionator, AuctionatorService } from '../../services/auctionator.service';
import { map, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import { ImportsSelectorService } from './imports-selector.service';

@Component({
  selector: 'app-imports-selector',
  templateUrl: './imports-selector.component.html',
  styleUrls: ['./imports-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportsSelectorComponent implements OnInit, OnDestroy {
  private readonly _destroy$ = new Subject();
  @Input() loader = false;
  @Output() importSelected = new EventEmitter<string>();
  auctionators$: Observable<Auctionator[]> = this.importsSelectorService.reloadImports.pipe(
    startWith(null),
    tap(() => {
      this.loader = true;
    }),
    takeUntil(this._destroy$),
    switchMap((token) => {
      return this.auctionatorService.getAll(); // default last 50
    }),
    map((auctionators) => {
      return auctionators.map((a) => {
        a.createdAt = new Date(a.createdAt);
        return a;
      });
    }),
    map((auctionators) => {
      // @ts-ignore
      return auctionators.sort((a, b) => b.createdAt - a.createdAt);
    }),
    tap(() => (this.loader = false))
  );

  constructor(private auctionatorService: AuctionatorService, private importsSelectorService: ImportsSelectorService) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  emitImport(auctionatorId: string) {
    this.importSelected.next(auctionatorId);
  }
}
