import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportsSelectorComponent } from './imports-selector.component';
import { NbFormFieldModule, NbSelectModule, NbSpinnerModule } from '@nebular/theme';

@NgModule({
  declarations: [ImportsSelectorComponent],
  exports: [ImportsSelectorComponent],
  imports: [CommonModule, NbFormFieldModule, NbSelectModule, NbSpinnerModule]
})
export class ImportsSelectorModule {}
