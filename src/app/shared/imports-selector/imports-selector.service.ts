import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImportsSelectorService {
  private readonly _reloadImports = new Subject();

  constructor() {}

  get reloadImports(): Subject<unknown> {
    return this._reloadImports;
  }
}
