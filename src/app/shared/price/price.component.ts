import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss']
})
export class PriceComponent implements OnChanges {
  @Input() price: number;
  @Input() loader: boolean;
  @Input() formControl: FormControl;
  @Output() valueChange = new EventEmitter();

  fixedPrice = this.fb.group({
    po: this.fb.control(''),
    pa: this.fb.control(''),
    pc: this.fb.control('')
  });

  constructor(private fb: FormBuilder) {}

  ngOnChanges(): void {
    if (this.price) {
      this.setForm(this.price);
    }
  }

  private setForm(price: number) {
    let po: number;
    let pa: number;
    let pc: number;
    pc = price % 100;

    if (price < 100) {
      this.fixedPrice.controls.pc.setValue(pc);
      return;
    }

    if (price < 10000) {
      pc = price % 100;
      pa = Math.floor(price / 100);

      this.fixedPrice.controls.pc.setValue(pc);
      this.fixedPrice.controls.pa.setValue(pa);
      return;
    }

    if (price >= 10000) {
      pa = Math.floor(price / 100);
      po = Math.floor(pa / 100);
      pa = pa % 100;
      this.fixedPrice.controls.pc.setValue(pc);
      this.fixedPrice.controls.pa.setValue(pa);
      this.fixedPrice.controls.po.setValue(po);
    }
  }

  get value() {
    const val = this.fixedPrice.value;
    return parseInt(val.po || 0, 10) * 10000 + parseInt(val.pa || 0, 10) * 100 + parseInt(val.pc || 0, 10);
  }

  formChange() {
    this.valueChange.emit(this.value);
  }
}
