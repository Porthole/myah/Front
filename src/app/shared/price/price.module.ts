import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PriceComponent } from './price.component';
import { NbFormFieldModule, NbIconModule, NbInputModule, NbSpinnerModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PriceComponent],
  imports: [CommonModule, NbFormFieldModule, ReactiveFormsModule, NbIconModule, NbInputModule, NbSpinnerModule],
  exports: [PriceComponent]
})
export class PriceModule {}
