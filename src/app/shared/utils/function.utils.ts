export function clearNullValues(obj) {
  const tmpObj = JSON.parse(JSON.stringify(obj));
  for (const key in tmpObj) {
    if (tmpObj[key] === undefined || tmpObj[key] === null) {
      delete tmpObj[key];
    } else if (typeof tmpObj[key] === 'object') {
      tmpObj[key] = clearNullValues(tmpObj[key]);
    }
  }
  return tmpObj;
}

export function toWowPriceString(price: number) {
  let po: number;
  let pa: number;
  let pc: number;
  if (price === 0) {
    return '';
  }

  if (price < 100) {
    return price + ' PC';
  }

  pc = price % 100;
  pa = Math.floor(price / 100);

  if (price < 10000) {
    return pa + ' PA ' + pc + ' PC';
  }

  po = Math.floor(pa / 100);
  pa = pa % 100;

  return po + ' PO ' + pa + ' PA ' + pc + ' PC';
}
