export const environment = {
  production: true,
  socket: {
    url: window.location.protocol + '//' + window.location.host,
    path: '/api/socket/'
  }
};
